package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.HashMap;
import java.util.Random;


public class Main extends Application {

    private static VBox rootMain = new VBox();
    private static VBox rootWord = new VBox();
    private static HBox rootChoice = new HBox();
    private static Button choiceDer = new Button("DER");
    private static Button choiceDie = new Button("DIE");
    private static Button choiceDas = new Button("DAS");
    private static Button NextWord = new Button("Weiter");
    private static Text wordField = new Text();
    private static HashMap wordArray = new HashMap();
    private static HashMap wordTrans = new HashMap();
    private static final Random random = new Random();


    @Override
    public void start(Stage primaryStage) throws Exception{
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("DER DIE DAS");
        fillWordArray();
        fillWordTrans();
        setRootMain();
        generateNewWord();
        initButtonHandler();
        primaryStage.setScene(new Scene(rootMain, 615, 135));


        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    private static void initButtonHandler(){
        choiceDer.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                //generateNewWord();
                if ("der"==wordArray.get(wordField.getText()))
                {
                    wordField.setText(wordArray.get(wordField.getText())+" "+wordField.getText()+"-"+wordTrans.get(wordField.getText()));
                    choiceDer.setStyle("-fx-background-color: Green");
                }
                else
                {
                    choiceDer.setStyle("-fx-background-color: Red");
                    choiceDer.setText("Falsch!");
                    choiceDer.setCancelButton(true);
                }

            }
        });

        choiceDie.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                //generateNewWord();
                if ("die"==wordArray.get(wordField.getText()))
                {
                    wordField.setText(wordArray.get(wordField.getText())+" "+wordField.getText()+"-"+wordTrans.get(wordField.getText()));
                    choiceDie.setStyle("-fx-background-color: Green");
                }
                else
                {
                    choiceDie.setStyle("-fx-background-color: Red");
                    choiceDie.setText("Falsch!");
                    choiceDie.setCancelButton(true);
                }
            }
        });

        choiceDas.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                //generateNewWord();

                if ("das"==wordArray.get(wordField.getText()))
                {
                    wordField.setText(wordArray.get(wordField.getText())+" "+wordField.getText()+"-"+wordTrans.get(wordField.getText()));
                    choiceDas.setStyle("-fx-background-color: Green");
                }
                else
                {
                    choiceDas.setStyle("-fx-background-color: Red");
                    choiceDas.setText("Falsch!");
                    choiceDas.setCancelButton(true);
                }
            }
        });

        NextWord.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                //generateNewWord();
                generateNewWord();
                setChoiceDer();
                setChoiceDie();
                setChoiceDas();

            }
        });

    }


    private static void setRootWord()
    {


        ///////!!!!!!!!!!!!!!!!!!

        rootWord.setAlignment(Pos.CENTER);
        rootWord.setSpacing(10);
        rootWord.getChildren().add(wordField);
    }


    private static void setRootChoice()
    {


        ///////!!!!!!!!!!!!!!!!!
        choiceDer.setMinSize(205, 50);
        choiceDie.setMinSize(205, 50);
        choiceDas.setMinSize(205, 50);
        setChoiceDer();
        setChoiceDie();
        setChoiceDas();

        rootChoice.setSpacing(0);
        rootChoice.getChildren().add(choiceDer);
        rootChoice.getChildren().add(choiceDie);
        rootChoice.getChildren().add(choiceDas);
    }

    private static void setChoiceDer()
    {
        choiceDer.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 25));
        choiceDer.setStyle("-fx-background-color: #009D91");
        choiceDer.setCancelButton(false);
        choiceDer.setText("DER");
    }

    private static void setChoiceDie()
    {
        choiceDie.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 25));
        choiceDie.setStyle("-fx-background-color: #009D91");
        choiceDie.setCancelButton(false);
        choiceDie.setText("DIE");
    }

    private static void setChoiceDas()
    {
        choiceDas.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 25));
        choiceDas.setStyle("-fx-background-color: #009D91");
        choiceDas.setCancelButton(false);
        choiceDas.setText("DAS");
    }

    private static void setNextWord()
    {
        NextWord.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 20));

        NextWord.setStyle("-fx-background-color:FF6F00");
        NextWord.setMinSize(615, 50);
    }
    private static void setRootMain()
    {
        setRootWord();
        setRootChoice();
        setNextWord();


        rootMain.setSpacing(0);
        rootMain.getChildren().add(rootWord);
        rootMain.getChildren().add(rootChoice);
        rootMain.getChildren().add(NextWord);
    }

    private static void generateNewWord()
    {
        int randN=random.nextInt(wordArray.size());
        String newWord= (String) wordArray.keySet().toArray()[randN];
        setWordField();
        wordField.setText(newWord);
    }

    private static void fillWordArray()
    {
        wordArray.put("Grafik","die");
        wordArray.put("Plastik","die");
        wordArray.put("Portrait","das");
        wordArray.put("Kunstler","der");
        wordArray.put("Himmel","der");
        wordArray.put("Raum","der");
        wordArray.put("Reise","die");
        wordArray.put("Brille","die");
        wordArray.put("Wolke","die");
        wordArray.put("Schall","der");
        wordArray.put("Bild","das");
        wordArray.put("Held","der");
    }

    private static void fillWordTrans()
    {
        wordTrans.put("Grafik","графика");
        wordTrans.put("Plastik","статуя");
        wordTrans.put("Portrait","портрет");
        wordTrans.put("Kunstler","врач");
        wordTrans.put("Himmel","небо");
        wordTrans.put("Raum","пространство");
        wordTrans.put("Reise","поездка,путешествие");
        wordTrans.put("Brille","очки");
        wordTrans.put("Wolke","облако,туча");
        wordTrans.put("Schall","звук");
        wordTrans.put("Bild","картина");
        wordTrans.put("Held","герой");
    }

    private static void setWordField()
    {
        wordField.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 30));

    }
}
